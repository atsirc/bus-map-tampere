# Map showing somewhat current locations of Tampere local buses  
For development reasons map updates every 10 seconds instead of 60.
Created with mapbox.com's map, mapbox-gl and react-map-gl packages. (maplibre-gl didnt work for some reason)
  
# Comment on how to use custom image as marker
I had a hard time getting custom marker to work. The only solutions I could find were in issue-threads on Github. Eventually I put it all together to a solution where I used Reacts useRef() and react-map-gl's Map component's onLoad prop.
  
```javascript
import marker from './assets/my-marker.png';

const mapRef = useRef();

const addMarker = () => {
  if (mapRef.current) {
    mapRef.current.loadImage(marker, (err, img) => {
      if (err) {
        throw err;
      } else {
        mapRef.current.addImage('custom-marker', img);
      }
    });
  }
};

const layerStyle = {
  id: 'points',
  type: 'symbol',
  layout: {
    'icon-image': 'custom-marker',
  }
  ...
};

...

return (
  <Map 
    ...
    ref={mapRef}
    onLoad={addMarker}
  >
    <Source id={layerStyle.id} ...>
      <Layer {...layerStyle} />
    </Source>
 </Map>
);

``` 
