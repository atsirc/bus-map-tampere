import './App.css';
import {useState, useEffect, useRef} from 'react';
import axios from 'axios';
import Map, {Source, Layer} from 'react-map-gl';
import marker from './assets/pngegg.png';

const url = 'https://data.itsfactory.fi/journeys/api/1/vehicle-activity';
const accessToken = process.env.REACT_APP_TOKEN;
const mapStyle = process.env.REACT_APP_MAP_STYLE;
const lng = 23.76;
const lat = 61.50;
const zoom = 9;

const  App = () => {
  const [source, setSource] = useState([]);
  const mapRef = useRef();

  useEffect(() => {
    getSource();
    const minute = setInterval(() => getSource(), 10000);
    return () =>  {
      clearInterval(minute);
    };
  }, []);

  const addMarker = () => {
    if (mapRef.current) {
      mapRef.current.loadImage(marker, (err, img) => {
        if (err) {
          throw err;
        } else {
          mapRef.current.addImage('custom-marker', img);
        }
      });
    }
  };

  const getSource = async () => {
    const result = await axios.get(url);

    const buses = result.data.body;

    const features = buses.map(bus => {
      const vehicle = bus.monitoredVehicleJourney;
      return {
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [
            vehicle.vehicleLocation.longitude, vehicle.vehicleLocation.latitude
          ]
        },
          properties: {
          title: vehicle.lineRef
          }
        };
    });

    const newSource = {
      type: 'FeatureCollection',
      features: features
    };

    setSource(newSource);
  };

  const layerStyle = {
    id: 'points',
    type: 'symbol',
    layout: {
      'icon-image': 'custom-marker',
      'icon-size': 0.5,
      'icon-optional': true,
      'icon-allow-overlap': true,
      'text-allow-overlap': true,
      'text-field': ['get', 'title'],
      'text-offset': [0,-1.8]
    }
  };

  return (
    <Map 
      initialViewState={{
        longitude: lng,
        latitude: lat,
        zoom: zoom,
      }}
      style={{width: '100vw', height: '100vh'}}
      minZoom={7}
      maxZoom={17}
      mapStyle={mapStyle}
      mapboxAccessToken={accessToken}
      onLoad={addMarker}
      ref={mapRef}
      reuseMaps
    >
      <Source id={layerStyle.id} type="geojson" data={source} cluster={true} clusterMaxZoom={11}>
        <Layer {...layerStyle} />
      </Source>
  </Map>);
};

export default App;
